﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class NodeManager : MonoBehaviour, PublicNodeManager {

	SocketIOComponent socket;

	bool connected = false;

	public void mStart ()
	{
		socket = GetComponent<SocketIOComponent>();

		// register events
		socket.On("open", OnConnected);
		socket.On("close", OnDisconnected);

		socket.On("stageUser", OnStageUser);
	}
	
	void OnConnected(SocketIOEvent e)
	{
		if (Managers.Debug)
		{
			Debug.Log("Connected to Node server...");
		}
		connected = true;
	}

	void OnDisconnected(SocketIOEvent e)
	{
		if (Managers.Debug)
		{
			Debug.Log("...Disconnected from Node server");
		}
		connected = false;
	}

	void OnStageUser(SocketIOEvent e)
	{
		Managers.Stage.ResetAndStage(e.data[0].ToString());
	}

	public void Ban(string _username, int _duration)
	{

		if (connected)
		{
			string banMessageData = string.Format(@"{{""username"":{0}, ""duration"":""{1}""}}", _username, _duration);
			if (Managers.Debug)
			{
				Debug.Log("Banning " + banMessageData);
			}
			socket.Emit("ban", new JSONObject(banMessageData));
		}
		else if (Managers.Debug)
		{
			Debug.Log("Banning of " + _username + " failed, not connected to Node server!");
		}
	}
}

public interface PublicNodeManager {
	void Ban(string _username, int _duration);
}